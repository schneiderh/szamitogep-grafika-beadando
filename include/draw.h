#ifndef DRAW_H
#define DRAW_H

#include <math.h>

#include <GL/glut.h>
#include <astro.h>

unsigned int texture_space;


//Pálya mutatásának ki/be kapcsolása
void toggle_draw_orbit();

//Fény ki be kapcsolása
void toggle_draw_light();

//Kocka rajzolása x,y,z-ben középpontozva
void draw_space(double x, double y, double z);

//[i]-edik bolygórol infó kiírás
void displaytext(astro *a, int i, int width, int height);

#endif /* DRAW_H */

