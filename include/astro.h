#ifndef ASTRO_H
#define ASTRO_H

#define TESTSCALE (1.0/16777216)
#define AMBIENTLIGHT

#ifndef M_PI
#define M_PI 3.14159265358979324
#endif

#include <stdbool.h>

bool flag_orbit;

typedef struct {
	char *name; //Az objektum neve
	int n; //Az objektum indexe
	float angleo, angler; //Rotáció és keringés szöge
	float radius, orbital_radius; //Sugár, keringési sugár
	float rotation_period, orbital_period; //Hány nap egy forgás és egy keringés
	float orbital_inclination, axial_tilt; //Fokok
	int n_child; //az objektumot objektumai, pl holdja
	unsigned int texture_id; // OpenGL textúra ID
	bool ring; // 1 (igaz) van gyűrűje, 0 (hamis) nincs gyűrűje
	unsigned int texture_id2; // gyűrű texture ID
	float ring_inner_radius, ring_outer_radius;
} astro;

astro *read_astro_data(const char *datafile, int *nastro);
void free_astro_data(astro *a, int n);

//Bolygópálya megrajzolás
void draw_orbit(float radius);

//Bolygógyűrű megrajzolása
void draw_ring(float inner_radius, float outer_radius);

//Bolygó rajzolás
int draw_astro(astro *a);
int draw_astro_select(astro *a);

#endif

