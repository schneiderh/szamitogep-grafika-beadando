#ifndef TEXTURE_H
#define TEXTURE_H

#include <GL/glut.h>

typedef GLubyte Pixel[3];

//textúra betöltés
GLuint load_texture(const char* filename);

#endif /* TEXTURE_H */

