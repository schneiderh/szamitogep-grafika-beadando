#ifndef CALLBACKS_H
#define CALLBACKS_H

#include <GL/glut.h>

//Mikor displayelni kell valamit
void display();

//Ha átméretezzük az ablakot
void reshape(int w, int h);

//Ha lenyomjuk az egeret
void mousefunc(int button, int state, int x, int y);

//Ha mozgatjuk az egérrel a camerát. pl jobb klikkel húzzúk el
void mousemove(int x, int y);

//Billentyű lenyomások metódusai
void keyboard(unsigned char key, int x, int y);
void specialkeys(int key, int x, int y);

//Idlefunction
void idlefunc();

#endif /* CALLBACKS_H */

