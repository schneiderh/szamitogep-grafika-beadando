all:
	gcc -Iinclude/ src/data.c src/astro.c src/callbacks.c src/draw.c src/init.c src/main.c src/sphere.c src/texture.c -lSOIL -static -static-libgcc -lglut32 -lopengl32 -lglu32 -lgdi32 -lwinmm -lm -o solar.exe -std=c99 -Wall -Wextra -Wpedantic

linux:
	gcc -Iinclude/ src/astro.c src/callbacks.c src/draw.c src/init.c src/main.c src/sphere.c src/texture.c -lSOIL -lGL -lGLU -lglut -lm -o solar -std=c99 -Wall -Wextra -Wpedantic
