#include <sphere.h>
#include <math.h>
#ifndef M_PI
#define M_PI 3.14159265358979324
#endif
#include <GL/gl.h>

int res_sphere = 0; 
double *sinth = 0, *costh = 0; 
double *sinph = 0, *cosph = 0;

int set_res_sphere(int n)
{
	int i;
	double angle;
	if (n < 4) n = 4;
	if (n == res_sphere) return -1;
	if (sinth) free(sinth);
	if (costh) free(costh);
	if (sinph) free(sinph);
	if (cosph) free(cosph);
	res_sphere = n;
	angle = M_PI/n;
	sinph = (double*)calloc(sizeof(double),n);
	cosph = (double*)calloc(sizeof(double),n);
	sinth = (double*)calloc(sizeof(double),n + 1);
	costh = (double*)calloc(sizeof(double),n + 1);
	if(!costh)
	{
		if (sinph) free(sinph);
		if (cosph) free(cosph);
		if (sinth) free(sinth);
		sinth = cosph = sinph = 0;
		return -1;
	}
	sinth[n] = sinth[0] = 0.0;
	costh[n] = costh[0] = 1.0;
	for (i = 1; i < n; i++)
	{
		sinth[i] = sin(-angle*(i<<1));
		costh[i] = cos(-angle*(i<<1));
		sinph[i] = sin(angle*i);
		cosph[i] = cos(angle*i);
	}
	return 0;
}



void draw_sphere(double radius)
{
	int i, j;
	double y0, y1, y0r, y1r;
	double r0, r1, r0r, r1r;
	float d = 1.0f/res_sphere;
	y1 = cosph[1];
	r1 = sinph[1];
	r1r = r1*radius;
	y1r = y1*radius;

	
	glBegin(GL_QUAD_STRIP);
		for (j = res_sphere;j >= 0; j--)
		{
			glNormal3d(sinth[j]*r1, y1, costh[j]*r1);
			glTexCoord2f(j*d, d);glVertex3d(sinth[j]*r1r, y1r, costh[j]*r1r);
			glNormal3d(0, 1, 0);
			glTexCoord2f(j*d, 0); glVertex3d(0, radius, 0);
		}
	glEnd();
	
	for (i = 2; i < res_sphere; i++)
	{
		y0 = y1; y1 = cosph[i];
		r0 = r1; r1 = sinph[i];
		y0r = y1r; y1r = y1*radius;
		r0r = r1r; r1r = r1*radius;
		glBegin(GL_QUAD_STRIP);
			for (j = 0; j <= res_sphere; j++)
			{
				glNormal3d(sinth[j]*r1, y1, costh[j]*r1);
				glTexCoord2f(j*d, i*d); glVertex3d(sinth[j]*r1r, y1r, costh[j]*r1r);
				glNormal3d(sinth[j]*r0, y0, costh[j]*r0);
				glTexCoord2f(j*d, (i - 1)*d); glVertex3d(sinth[j]*r0r, y0r, costh[j]*r0r);
			}
		glEnd();
	}

	glBegin(GL_QUAD_STRIP);
		for(j = 0; j <= res_sphere; j++)
		{
			glNormal3d(0,-1,0);
			glTexCoord2f(j*d, 1); glVertex3d(0, -radius, 0);
			glNormal3d(sinth[j]*r1, y1, costh[j]*r1);
			glTexCoord2f(j*d, 1 - d); glVertex3d(sinth[j]*r1r, y1r, costh[j]*r1r);
		}
	glEnd();
}

