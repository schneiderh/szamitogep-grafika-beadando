#include <draw.h>
#include <callbacks.h>
#include <astro.h>
#include <draw.h>
#include <data.h>

double eyeX = 0.0, eyeZ = 0.0, eyeY = 3.0E8*TESTSCALE;
double forwardX = 0.0, forwardY = -1.0, forwardZ = 0.0;
double upX = 0.0, upZ = 1.0, upY = 0.0;
double rightX = -1.0, rightY = 0.0, rightZ = 0.0;

#define BUFSIZE 512
int Selected_Object = -1;
void pick_object(int x, int y)
{
	GLuint selectBuf[BUFSIZE];
	GLint hits, j;
	GLint viewport[4];

	glGetIntegerv(GL_VIEWPORT, viewport);

	glSelectBuffer(BUFSIZE, selectBuf);
	(void)glRenderMode(GL_SELECT);

	glInitNames();
	glPushName(0);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	//5*5-ös klikkelhető felület
	gluPickMatrix((GLdouble) x, (GLdouble) (viewport[3] - y), 5.0, 5.0, viewport);
	gluPerspective(45.0, (float)viewport[2]/viewport[3], 1.0E6*TESTSCALE, 3.0E10*TESTSCALE);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(eyeX, eyeY, eyeZ, eyeX + forwardX, eyeY + forwardY, eyeZ + forwardZ, upX, upY, upZ);
	draw_astro_select(object);
	glFlush();
	/***************/

	glMatrixMode(GL_PROJECTION);
	glPopMatrix ();

	hits = glRenderMode(GL_RENDER);
	if(hits > 0)
	{
		j = 0;
		while(--hits)
		{
			if(selectBuf[j*4 + 1] > selectBuf[hits*4 + 1]) j = hits;
		}
		Selected_Object = selectBuf[j*4 + 3];
	}
	else
		Selected_Object = -1;
	reshape(viewport[2], viewport[3]);
} 

int window_width, window_height;

void reshape(int w, int h)
{
	window_height = h;
	window_width = w;
  glViewport(0, 0, (GLsizei)w, (GLsizei)h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, (float)w/h, 1.0E6*TESTSCALE, 3.0E10*TESTSCALE);
  glMatrixMode(GL_MODELVIEW);
}

int flag_mousemove = 0;
int flag_right = 0, flag_up = 0;
int x_old, y_old;
void mousemove(int x, int y)
{
	if (flag_mousemove)
	{
		if (x - x_old < -10)
			flag_right = -1;
		else if (x - x_old > 10)
			flag_right = 1;
		else
			flag_right = 0;
		if (y - y_old > 10)
			flag_up = 1;
		else if (y - y_old < -10)
			flag_up = -1;
		else
			flag_up = 0;
	}
}

void mousefunc(int button, int state, int x, int y)
{
	if (state == GLUT_DOWN)
	{
		if (button == GLUT_RIGHT_BUTTON)
		{
			flag_mousemove = 1;
			x_old = x;
			y_old = y;
		}
		else if (button == GLUT_LEFT_BUTTON)
			pick_object(x, y);
	}
	else
		flag_mousemove = flag_up = flag_right = 0;
}

float time_step = 0.02;
#define COS_A cos(0.1*M_PI/180)
#define SIN_A sin(0.1*M_PI/180)
void idlefunc()
{
	int i;
	double x = forwardX, y = forwardY, z = forwardZ;
	for (i = 0; i < NumObjects; i++)
	{
		object[i].angleo += time_step*(2*M_PI)/object[i].orbital_period;
		object[i].angler += time_step*(2*M_PI)/object[i].rotation_period;
	}

	if (flag_right > 0)
	{
		forwardX = x*COS_A + rightX*SIN_A;
		forwardY = y*COS_A + rightY*SIN_A;
		forwardZ = z*COS_A + rightZ*SIN_A;
		rightX = rightX*COS_A - x*SIN_A;
		rightY = rightY*COS_A - y*SIN_A;
		rightZ = rightZ*COS_A - z*SIN_A;
	}
	else if (flag_right < 0)
	{
		forwardX = x*COS_A - rightX*SIN_A;
		forwardY = y*COS_A - rightY*SIN_A;
		forwardZ = z*COS_A - rightZ*SIN_A;
		rightX = rightX*COS_A + x*SIN_A;
		rightY = rightY*COS_A + y*SIN_A;
		rightZ = rightZ*COS_A + z*SIN_A;	
	}
	if (flag_up && flag_right)
	{
		x = forwardX;
		y = forwardY;
		z = forwardZ;
	}
	if (flag_up > 0)
	{
		forwardX = x*COS_A - upX*SIN_A;
		forwardY = y*COS_A - upY*SIN_A;
		forwardZ = z*COS_A - upZ*SIN_A;
		upX = upX*COS_A + x*SIN_A;
		upY = upY*COS_A + y*SIN_A;
		upZ = upZ*COS_A + z*SIN_A;	
	}
	else if (flag_up < 0)
	{
		forwardX = x*COS_A + upX*SIN_A;
		forwardY = y*COS_A + upY*SIN_A;
		forwardZ = z*COS_A + upZ*SIN_A;
		upX = upX*COS_A - x*SIN_A;
		upY = upY*COS_A - y*SIN_A;
		upZ = upZ*COS_A - z*SIN_A;	
	}
	glutPostRedisplay();
}

void light_intensity(int updown)
{
#ifdef AMBIENTLIGHT
	static float d = 0.1f;
#else
	static float d = 1.0f;
#endif
	float light[4] = {1.0f, 1.0f, 1.0f, 1.0f};
	if (d > 0.0f && updown < 0)
		d -= 0.03125;
	else if (d < 1.0f && updown > 0)
		d += 0.03125;
	light[0] = light[1] = light[2] = d;
#ifdef AMBIENTLIGHT
	glLightfv(GL_LIGHT0, GL_AMBIENT, light);
#else
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light);
#endif
}

double step = 1.0E6*TESTSCALE;
void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
		case 'w':
			eyeX += step*forwardX;
			eyeY += step*forwardY;
			eyeZ += step*forwardZ;
			break;
		case 's':
			eyeX -= step*forwardX;
			eyeY -= step*forwardY;
			eyeZ -= step*forwardZ;
			break;
		case 'a':
			eyeX -= step*rightX;
			eyeY -= step*rightY;
			eyeZ -= step*rightZ;
			break;
		case 'd':
			eyeX += step*rightX;
			eyeY += step*rightY;
			eyeZ += step*rightZ;
			break;
		case 'o':
			toggle_draw_orbit();
			break;
		case 'l':
			toggle_draw_light();
			break;
		case '+':
			light_intensity(1);
			break;
		case '-':
			light_intensity(-1);
			break;
	}
	glutPostRedisplay();
}


void specialkeys(int key, int x, int y)
{
	switch (key)
	{
		case GLUT_KEY_F1:
			if (Selected_Object == -2)
				Selected_Object = -1;
			else
				Selected_Object = -2;
			break;
		case GLUT_KEY_RIGHT:
			time_step *= 1.4;
			break;
		case GLUT_KEY_LEFT:
			time_step /= 1.4;
			break;
		case GLUT_KEY_UP:
			step *= 1.4;
			break;
		case GLUT_KEY_DOWN:
			step /= 1.4;
			break;
	}
}


void display()
{
	float light0_position[4] = {0, 0, 0, 0}; 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if (Selected_Object != -1)
		displaytext(object, Selected_Object, window_width, window_height);
	glLoadIdentity();
	glLightfv(GL_LIGHT0, GL_POSITION, light0_position);
	gluLookAt(eyeX, eyeY, eyeZ, eyeX + forwardX, eyeY + forwardY, eyeZ + forwardZ, upX, upY, upZ);
	draw_space(eyeX, eyeY, eyeZ);
	draw_astro(object);
	glutSwapBuffers();
}

