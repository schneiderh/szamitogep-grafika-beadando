#include <data.h>
#include <astro.h>

int read_data(const char *filename)
{
	object = read_astro_data(filename, &NumObjects);
	return object == 0;
}