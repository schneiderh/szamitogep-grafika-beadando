#include <stdio.h>
#include <draw.h>

#define HLCUBE 2.0E10*TESTSCALE
void draw_space(double x, double y, double z)
{
	glColor4f(1, 1, 1, 1);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture_space);
	glBegin(GL_QUADS);
	//eleje
	glTexCoord2f(0.25, 1.0/3); glVertex3f(x - HLCUBE, y + HLCUBE, z - HLCUBE);
	glTexCoord2f(0.5, 1.0/3); glVertex3f(x + HLCUBE, y + HLCUBE, z - HLCUBE);
	glTexCoord2f(0.5, 2.0/3); glVertex3f(x + HLCUBE, y - HLCUBE, z - HLCUBE);
	glTexCoord2f(0.25, 2.0/3); glVertex3f(x - HLCUBE, y - HLCUBE, z - HLCUBE);
	//hátulja
	glEnd();
	glBegin(GL_QUADS);
	glTexCoord2f(1, 1.0/3); glVertex3f(x - HLCUBE, y + HLCUBE, z + HLCUBE);
	glTexCoord2f(0.75, 1.0/3); glVertex3f(x + HLCUBE, y + HLCUBE, z + HLCUBE);
	glTexCoord2f(0.75, 2.0/3); glVertex3f(x + HLCUBE, y - HLCUBE, z + HLCUBE);
	glTexCoord2f(1, 2.0/3); glVertex3f(x - HLCUBE, y - HLCUBE, z + HLCUBE);
	glEnd();
	//bal
	glBegin(GL_QUADS);
	glTexCoord2f(0, 1.0/3); glVertex3f(x - HLCUBE, y + HLCUBE, z + HLCUBE);
	glTexCoord2f(0.25, 1.0/3); glVertex3f(x - HLCUBE, y + HLCUBE, z - HLCUBE);
	glTexCoord2f(0.25, 2.0/3); glVertex3f(x - HLCUBE, y - HLCUBE, z - HLCUBE);
	glTexCoord2f(0, 2.0/3); glVertex3f(x - HLCUBE, y - HLCUBE, z + HLCUBE);
	glEnd();
	//jobb
	glBegin(GL_QUADS);
	glTexCoord2f(0.5, 1.0/3); glVertex3f(x + HLCUBE, y + HLCUBE, z - HLCUBE);
	glTexCoord2f(0.75, 1.0/3); glVertex3f(x + HLCUBE, y + HLCUBE, z + HLCUBE);
	glTexCoord2f(0.75, 2.0/3); glVertex3f(x + HLCUBE, y - HLCUBE, z + HLCUBE);
	glTexCoord2f(0.5, 2.0/3); glVertex3f(x + HLCUBE, y - HLCUBE, z - HLCUBE);
	glEnd();
	//fent
	glBegin(GL_QUADS);
	glTexCoord2f(0.25, 1.0/3); glVertex3f(x - HLCUBE, y + HLCUBE, z - HLCUBE);
	glTexCoord2f(0.5, 1.0/3); glVertex3f(x + HLCUBE, y + HLCUBE, z - HLCUBE);
	glTexCoord2f(0.5, 0); glVertex3f(x + HLCUBE, y + HLCUBE, z + HLCUBE);
	glTexCoord2f(0.25, 0); glVertex3f(x - HLCUBE, y + HLCUBE, z + HLCUBE);
	glEnd();
	//hátulja
	glBegin(GL_QUADS);
	glTexCoord2f(0.25, 2.0/3); glVertex3f(x - HLCUBE, y - HLCUBE, z - HLCUBE);
	glTexCoord2f(0.5, 2.0/3); glVertex3f(x + HLCUBE, y - HLCUBE, z - HLCUBE);
	glTexCoord2f(0.5, 1); glVertex3f(x + HLCUBE, y - HLCUBE, z + HLCUBE);
	glTexCoord2f(0.25, 1); glVertex3f(x - HLCUBE, y - HLCUBE, z + HLCUBE);
	glEnd();
	glDisable(GL_TEXTURE_2D);
}


void drawstring(const char *s, int x, int y)
{
	glRasterPos2i(x, y);
	while (*s)
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15,*(s++));
}

void displaytext(astro *a, int i, int width, int height)
{
	char s[256];
	glPushMatrix();
	glLoadIdentity(); 

	glMatrixMode(GL_PROJECTION); 
	glPushMatrix(); 
	glLoadIdentity();
	gluOrtho2D(0, width, -height, 0);

	glColor3ub(255, 255, 255);
	if (i >= 0)
	{
		drawstring(a[i].name, 20, -20);
		sprintf(s, "Atmero: %g km", a[i].radius*(i? 0.002: 0.05)/TESTSCALE);
		drawstring(s, 40, -50);
		sprintf(s, "Forgasi idoszak: %g nap", a[i].rotation_period);
		drawstring(s, 40, -70);
		sprintf(s, "Tengely dolese: %g fok", a[i].axial_tilt);
		drawstring(s, 40, -90);
		if (i) //Nem a napra
		{
			sprintf(s, "Palya sugara: %g km", a[i].orbital_radius*(i == 4? 0.02: 1)/TESTSCALE);
			drawstring(s, 40, -110);
			sprintf(s, "Palya megtetel ideje: %g nap", a[i].orbital_period);
			drawstring(s, 40, -130);
			sprintf(s, "Palya dolese: %g fok", a[i].orbital_inclination);
			drawstring(s, 40, -150);
		}
	}
	else
	{
		drawstring("BILLENTYUPARANCSOK", 20, -20);
		drawstring("W: Elore mozgas", 20, -40);
		drawstring("S: Hatra mozgas", 220, -40);
		drawstring("A: Balra mozgas", 20, -60);
		drawstring("D: Jobbra mozgas", 220, -60);
		drawstring("O: Palya megjelenites ki es bekapcsolasa", 20, -80);
		drawstring("F1: Sugo, avagy mi mit csinal", 20, -100);
		drawstring("+/-: Fenyero noveles/csokkentes", 20, -120);
		drawstring("Bal/Jobb nyil: Ido mulasanak gyorsasaganak növelese/csokkentese", 20, -140);
		drawstring("Fel/Le nyíl: Mozgasi sebesseg novelese/csokkentese", 20, -160);
		drawstring("EGER", 20, -190);
		drawstring("Bal egergomb: Objektum kivalasztasa", 20, -210);
		drawstring("Jobb eger + eger mozgatas: Kamera rotalas", 20, -230);
		drawstring("Keszitette: Schneider Henrik - NRTHZZ", 20, -250);
	}
	glPopMatrix(); 

	glMatrixMode(GL_MODELVIEW); 
	glPopMatrix(); 
}

