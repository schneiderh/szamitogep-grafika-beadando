#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <texture.h>
#include <callbacks.h>
#include <init.h>
#include <sphere.h>
#include <SOIL/SOIL.h>
#include <draw.h>
#include <data.h>

void set_callbacks()
{
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
		glutSpecialFunc(specialkeys);
    glutIdleFunc(idlefunc);
		glutMouseFunc(mousefunc);
		glutMotionFunc(mousemove);
}

int main(int argc, char* argv[])
{
	glutInit(&argc, argv);

	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(640, 480);     

	glutCreateWindow("Solar System");

	set_callbacks();
	init_opengl();
	if (set_res_sphere(36))
		return 0;
	texture_space = load_texture("0-space-skybox.png");
	if (read_data("models/solar.dat"))
	{
		printf("Error reading data\n");
		return 0;
	}
	glutMainLoop();

	return 0;
}



