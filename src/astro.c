#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <astro.h>
#include <texture.h>
#include <sphere.h>
#define ORBIT_SIDES 120

void free_astro_data(astro *a, int n)
{
	int i;
	if (a)
	{
		for (i = 0; i < n; i++)
		{
			if (!a[i].name)
				break;
			free(a[i].name);
		}
		free (a);
	}
}

astro *read_astro_data(const char *datafile, int *nastro)
{
  FILE *f;
	astro *a;
	char s[256];
  int i;

  f = fopen(datafile, "r");
  if (!f)
		return 0;
	fgets(s, 255, f);
  fscanf(f, "%d", nastro);
	a = (astro*)malloc(*nastro*sizeof(astro));
  if (!f)
	{
		fclose(f);
		return 0;
	}

  for (i = 0; i < *nastro; i++)
	{
		a[i].n = i;
    fscanf(f, "%s %f %f %f %f %f %f %f %f %d",
      s,
      &a[i].angleo, &a[i].angler,
      &a[i].radius, &a[i].orbital_radius,
      &a[i].rotation_period, &a[i].orbital_period,
      &a[i].orbital_inclination, &a[i].axial_tilt,
			&a[i].n_child);
		a[i].radius *= (i? 500: 20)*TESTSCALE;
		a[i].orbital_radius *= TESTSCALE;
		if (i == 4)
			a[i].orbital_radius *= 50; 
		a[i].name = (char*)malloc(strlen(s) + 1);
		if (!a[i].name)
		{
			fclose(f);
			return 0;
		}
		strcpy(a[i].name, s);
		fscanf(f, "%s %d", s, &a[i].ring);
		a[i].texture_id = load_texture(s);
		if (a[i].ring)
		{
			fscanf(f, "%s %f %f", s, &a[i].ring_inner_radius, &a[i].ring_outer_radius);
			a[i].texture_id2 = load_texture(s);
			a[i].ring_inner_radius *= 500*TESTSCALE;
			a[i].ring_outer_radius *= 500*TESTSCALE;
		}
	}	
  fclose(f);
	return a;
}

void draw_orbit(float radius)
{
	int i;
	float angle = 0, delta = M_PI*2.0/ORBIT_SIDES;
	glColor3ub(180,180,180);
	glBegin(GL_LINE_LOOP);
	for (i = 0; i < ORBIT_SIDES; ++i)
	{
		glVertex3f(radius*cos(angle), 0, radius*sin(angle));
		angle += delta;
	}
	glEnd();
}


void draw_ring(float inner_radius, float outer_radius)
{
	int i;
	float angle = 0, delta = M_PI*2.0/ORBIT_SIDES;
	glColor3ub(255,255,255);
	glBegin(GL_TRIANGLE_STRIP);
	for (i = 0; i <= ORBIT_SIDES; ++i)
	{
		glTexCoord2f(0, 0);
		glVertex3f(inner_radius*cos(angle), 0, inner_radius*sin(angle));
		glTexCoord2f(1, 1);
		glVertex3f(outer_radius*cos(angle), 0, outer_radius*sin(angle));
		angle += delta;
	}
	glEnd();
}

void toggle_draw_orbit()
{
	flag_orbit = !flag_orbit;
}

int flag_light = 1;

void toggle_draw_light()
{
	flag_light = !flag_light;
}

int draw_astro(astro *a)
{
	int i, n = 1;
	glPushMatrix();
		if (a->orbital_radius != 0.0)
		{
			glRotatef(a->orbital_inclination, 0, 0, 1);
			if (flag_orbit)
				draw_orbit(a->orbital_radius);
			glTranslatef(a->orbital_radius*cos(a->angleo), 0, a->orbital_radius*sin(a->angleo));
		}
		if (a->n_child != 0)
		{
			for (i = 0; i < a->n_child; i++) 
				n += draw_astro(a + n);
		}
		glPushMatrix();
			glRotatef(a->axial_tilt, 0, 0, 1);
			glEnable(GL_TEXTURE_2D);
			if (a->ring)
			{
				glBindTexture(GL_TEXTURE_2D, a->texture_id2);
				draw_ring(a->ring_inner_radius, a->ring_outer_radius);
			}
			glRotatef(a->angler, 0, 1, 0); 
			glBindTexture(GL_TEXTURE_2D, a->texture_id);
			if (a->n && flag_light)
			{
				glEnable(GL_LIGHTING);
				draw_sphere(a->radius);
				glDisable(GL_LIGHTING);
			}
			else
				draw_sphere(a->radius);
			glDisable(GL_TEXTURE_2D);
		glPopMatrix();
	glPopMatrix();
	return n;
}

int draw_astro_select(astro *a)
{
	int i, n = 1;
	glPushMatrix();
		if (a->orbital_radius != 0.0)
		{
			glRotatef(a->orbital_inclination, 0, 0, 1);
			glTranslatef(a->orbital_radius*cos(a->angleo), 0, a->orbital_radius*sin(a->angleo));
		}
		if (a->n_child != 0)
		{
			for (i = 0; i < a->n_child; i++)
				n += draw_astro_select(a + n);
		}
	glLoadName((unsigned int)a->n);
	glutSolidSphere(a->radius, 16, 16);
	glPopMatrix();
	return n;
}
